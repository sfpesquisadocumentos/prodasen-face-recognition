#!/bin/sh

FACENET_DIR=../dev/facenet
LFW_DATASET_DIR=../data/datasets/lfw/lfw_mtcnnpy_160/
PRE_TRAINED_MODEL_DIR=../data/pre-trained-models/20170512-110547/

#/datasets/senadores/senadores-aligned-mtcnn/

if [ $# -eq 0 ]; then
	echo "Script será executado com os valores padrão:"
else 
	FACENET_DIR=$1
	LFW_DATASET_DIR=$2
	PRE_TRAINED_MODEL_DIR=$3
	echo "Script será executado com os parâmetros de entrada:"
fi

echo "FACENET_DIR="$FACENET_DIR"; LFW_DATASET_DIR="$LFW_DATASET_DIR"; PRE_TRAINED_MODEL_DIR="$PRE_TRAINED_MODEL_DIR

python 	$FACENET_DIR/src/validate_on_lfw.py \
	$LFW_DATASET_DIR \
	$PRE_TRAINED_MODEL_DIR \
	--lfw_pairs $FACENET_DIR/data/pairs.txt
