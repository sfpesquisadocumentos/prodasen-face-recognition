#!/bin/bash

SENADORES_DATASET=../data/datasets/senadores/senadores-teste-dataset/*

for SENADOR_DATASET in $SENADORES_DATASET
do
	OUTPUT="$SENADOR_DATASET/"inferencia.out

	QTD_IMAGENS_SENADOR=$(ls "$SENADOR_DATASET"/* | wc -l)
	COUNTER=0

	while read linha;
	do
		echo $linha
	done < "$OUTPUT"
done
