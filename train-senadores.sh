#!/bin/sh


#for N in {1..128}; do
	python ./facenet/src/facenet_train.py \
        	--logs_base_dir ./facenet_logs/ \
	        --models_base_dir ./facenet_models/ \
#		--gpu_memory_fraction \
		--pretrained_model ./facenet_pre-trained-models/ \
        	--data_dir ./senadores/ \
        	--model_def models.nn4 \ #models.inception_resnet_v1 \
		--max_nrof_epochs 80 \
		--batch_size 90 \
		--image_size 160 \
		--people_per_batch 45 \ 
		--lfw_dir ./datasets/lfw/lfw_mtcnnpy_160 \
	        --optimizer RMSPROP \
        	--learning_rate -1 \
	        --max_nrof_epochs 80 \
		--batch_size 90 \ 
        	--keep_probability 0.8 \
	        --random_crop \
        	--random_flip \
	        --learning_rate_schedule_file ./facenet/data/learning_rate_schedule_classifier_casia.txt \
        	--weight_decay 5e-5 \
	        --center_loss_factor 1e-4 \
        	--center_loss_alfa 0.9 #& done
