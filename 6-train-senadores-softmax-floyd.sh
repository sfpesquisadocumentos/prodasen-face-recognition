#!/bin/sh

floyd init face-recognition

floyd run --gpu --env tensorflow-1.2:py2 \
	--data prodasen/datasets/facenet/1:facenet \
	--data prodasen/datasets/lfw_mtcnnpy_160/1:lfw \
	--data prodasen/datasets/pre-trained-models/1:pre-trained-models \
	--data prodasen/datasets/senadores-aligned-mtcnn/2:senadores \
	--tensorboard \
	"bash 5-train-senadores-softmax-local.sh /facenet /lfw /senadores /output"
