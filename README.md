# README #

Este repositório contém os arquivos necessários para rodar o treinamento do reconhecimento de face dos Senadores do Brasil

### What is this repository for? ###

Este repositório contém os seguintes arquivos:

* Dockerfile: para construir uma imagem Docker com a compilação do tensorflow e todos as dependências necessárias. Além do facenet utilizado para executar o treinamento e reconhecimento de faces;
* hello_tensorflow.py: script para testar se a compilação e a instalação do tensorflow foi bem sucedida;
* train-senadores.sh: script que chama o script facenet_train_classifier.py passando os parâmetros necessários
* Senadores-aligned-mtcnn: diretório de imagens dos Senadores para treinamento após aplicação do MTCNN.

### How do I get set up? ###

* Summary of set up
Clonar o repositório

Executar: sudo docker build -t compiled-tensorflow-facenet .

Executar: sudo docker run --name container_compiled-tensorflow-facenet -it -p 8888:8888 -p 6006:6006 -v /home/ffsant/_tasks/201704_grupo_pesquisa/prodasen-face-recognition/Senadores-aligned-mtcnn:/home/prodasen/senadores -v /home/ffsant/_tasks/201704_grupo_pesquisa/prodasen-face-recognition/pre-trained-models:/home/prodasen/facenet_pre-trained-models compiled-tensorflow-facenet

Acessar o console do docker e executar: ./train-senadores.sh a partir do diretório prodasen

* Dependencies
Git

Docker