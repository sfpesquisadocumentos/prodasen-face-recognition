#!/bin/bash
SENADORES_DIR=../data/datasets/senadores
PROCESSADO_DIR="$SENADORES_DIR"/processado
QUARENTENA_DIR="$SENADORES_DIR"/quarentena
TEST_DATASET="$SENADORES_DIR"/test-dataset

find $PROCESSADO_DIR -type f -name *.out -delete
find $QUARENTENA_DIR -type f -name *.out -delete

for SENADOR_DIR in "$PROCESSADO_DIR"/*
do

	NOME_SENADOR=$(echo $SENADOR_DIR | rev | cut -d "/" -f 1 | rev)

	#echo "$SENADOR_DIR - $TEST_DATASET/$NOME_SENADOR"
	mv "$SENADOR_DIR"/75/* "$TEST_DATASET/$NOME_SENADOR" 2>/dev/null
	mv "$SENADOR_DIR"/50/* "$TEST_DATASET/$NOME_SENADOR" 2>/dev/null
	mv "$SENADOR_DIR"/00/* "$TEST_DATASET/$NOME_SENADOR" 2>/dev/null

done

for SENADOR_DIR in "$QUARENTENA_DIR"/*
do

        NOME_SENADOR=$(echo $SENADOR_DIR | rev | cut -d "/" -f 1 | rev)

        #echo "$SENADOR_DIR - $TEST_DATASET/$NOME_SENADOR"
        mv "$SENADOR_DIR"/* "$TEST_DATASET/$NOME_SENADOR" 2>/dev/null
done

echo "Arquivos movidos prontos para serem reprocessados:"
find "$TEST_DATASET" -type f | wc -l

find "$PROCESSADO_DIR" -type f | wc -l
find "$QUARENTENA_DIR" -type f | wc -l

