#!/bin/sh

FACENET_DIR=../dev/facenet/src
MODEL_DIR_TO_FREEZE=../data/output/floyd/facenet_models/20170915-195910
FROZEN_MODEL_OUTPUT=$MODEL_DIR_TO_FREEZE/senadores-10k-floyd-20170915.pb

python $FACENET_DIR/freeze_graph.py \
	$MODEL_DIR_TO_FREEZE \
	$FROZEN_MODEL_OUTPUT
