FROM ubuntu:latest

#ENV http_proxy 'http://10.0.2.15:3128/'
#ENV https_proxy 'http://10.0.2.15:3128/'

###################################
#TENSORFLOW CONFIGURATION VARIABLES
###################################
ENV	PYTHON_BIN_PATH=/usr/bin/python2.7 \
	CC_OPT_FLAGS=-march=native \
	TF_NEED_JEMALLOC=1 \
	TF_NEED_GCP=0 \
	TF_NEED_HDFS=0 \
	TF_ENABLE_XLA=0 \
	TF_NEED_OPENCL=0 \
	TF_NEED_CUDA=0 \
	GCC_HOST_COMPILER_PATH=/usr/bin/gcc \
	PYTHON_LIB_PATH=/usr/local/lib/python2.7/dist-packages \
	TF_NEED_VERBS=0 \
	TF_NEED_MPI=0 \
	TF_NEED_GDR=0 \
	TF_NEED_MKL=0

############
#SET WORKDIR
############
RUN cd /home

RUN	mkdir /home/prodasen && \
	mkdir /home/prodasen/tensorflow && \
	mkdir /home/prodasen/facenet && \
	mkdir /home/prodasen/facenet_pre-trained-models && \
        mkdir /home/prodasen/facenet_models && \
        mkdir /home/prodasen/facenet_logs && \
        mkdir /home/prodasen/senadores

WORKDIR /home/prodasen/tensorflow

#######################
#CONFIGURE LOCALE pt_BR
#######################

RUN	apt-get -y update && \
	apt-get -y install locales

RUN locale-gen pt_BR.UTF-8

ENV	LANG=pt_BR.UTF-8 \
	LANGUAGE=pt_BR.UTF-8 \
	LC_ALL=pt_BR.UTF-8

############################
#UPDATE AND INSTALL PACKAGES
############################
RUN	apt-get -y update && \
	apt-get -y upgrade && \
	apt-get -y install \
		git \
		curl \
		python-pip \
		python-numpy \
		python-dev && \
	apt-get -y update && \
	apt-get -y install python-wheel && \
	apt-get -y update && \
	pip install --upgrade pip && \
	pip install scipy \
		scikit-learn \
		opencv-python \
		h5py \
		matplotlib \
		Pillow \
		requests \
		psutil

##############
#INSTALL BAZEL
##############
RUN echo "deb [arch=amd64] http://storage.googleapis.com/bazel-apt stable jdk1.8" | tee /etc/apt/sources.list.d/bazel.list

RUN curl https://bazel.build/bazel-release.pub.gpg | apt-key add -

RUN apt-get update 
RUN apt-get -y install openjdk-8-jdk
RUN apt-get update
RUN apt-get -y install bazel 
RUN apt-get -y upgrade bazel

####################
#INSTALL TENSORFLOW
####################
RUN git clone https://github.com/tensorflow/tensorflow /home/prodasen/tensorflow && \
        git checkout r1.2

RUN ./configure

RUN bazel build --config=opt //tensorflow/tools/pip_package:build_pip_package

RUN bazel-bin/tensorflow/tools/pip_package/build_pip_package /tmp/tensorflow_pkg

RUN pip install /tmp/tensorflow_pkg/tensorflow-1.2.1-cp27-cp27mu-linux_x86_64.whl

#########################
#CLONE FACENET REPOSITORY
#########################
RUN git clone https://github.com/davidsandberg/facenet.git /home/prodasen/facenet

ENV PYTHONPATH=/home/prodasen/facenet/src

###############################
#SCRIPT TO RUN FACENET TRAINING
###############################
RUN mkdir /home/prodasen/scripts

ADD 1-get-lfw-dataset.sh /home/prodasen/scripts
ADD 2-align-lfw-dataset.sh /home/prodasen/scripts
ADD 3-validate-on-lfw.sh /home/prodasen/scripts
ADD 4-compare-senadores.sh /home/prodasen/scripts
ADD 5-train-senadores-softmax-local.sh /home/prodasen/scripts
ADD 6-train-senadores-softmax-floyd.sh /home/prodasen/scripts
ADD 9-start-tensorboard.sh /home/prodasen/scripts
ADD hello_tensorflow.py /home/prodasen/scripts

############################
#TEST TENSORFLOW INSTALATION
############################
RUN python /home/prodasen/scripts/hello_tensorflow.py

#############
#EXPOSE PORTS
#############
EXPOSE 8888
EXPOSE 6006

