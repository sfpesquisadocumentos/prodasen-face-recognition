#!/bin/sh

$DATASET_DIR=../data/datasets

wget -P $DATASET_DIR http://vis-www.cs.umass.edu/lfw/lfw.tgz

cd $DATASET_DIR

mkdir -p lfw/raw
mkdir -p lfw/lfw_mtcnnpy_160

tar xvf lfw.tgz -C ./lfw/raw --strip-components=1

rm lfw.tgz
