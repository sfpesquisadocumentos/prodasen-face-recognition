#!/bin/sh

FACENET_DIR=../dev/facenet
LFW_DATASET_DIR=../data/datasets/lfw

export PYTHONPATH=$PYTHONPATH:../dev/facenet/src/

#for N in {1..8}; do
	python $FACENET_DIR/src/align/align_dataset_mtcnn.py \
		$LFW_DATASET_DIR/raw \
		$LFW_DATASET_DIR/lfw_mtcnnpy_160 \
		--image_size 160 \
		--margin 32 \
		--random_order #& done \
		#--gpu_memory_fraction 0.25 & done
