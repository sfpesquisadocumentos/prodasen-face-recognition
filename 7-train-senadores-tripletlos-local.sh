#!/bin/sh
if [ $# -eq 0 ]; then
        echo ">>> Script será executado com os valores padrão:"
	FACENET_DIR=../dev/facenet
	DATASET_LFW_DIR=../data/datasets/lfw/lfw_mtcnnpy_160
	DATASET_SENADORES_DIR=../data/datasets/senadores/senadores-aligned-mtcnn
	OUTPUT_DIR=../data/output
else
        FACENET_DIR=$1
        DATASET_LFW_DIR=$2
        DATASET_SENADORES_DIR=$3
        OUTPUT_DIR=$4
        echo ">>> Script será executado com os parâmetros de entrada:"
fi

echo ">>>>> FACENET_DIR: "$FACENET_DIR
echo ">>>>> DATASET_LFW_DIR: "$DATASET_LFW_DIR
echo ">>>>> DATASET_SENADORES_DIR: "$DATASET_SENADORES_DIR
echo ">>>>> OUTPUT_DIR: "$OUTPUT_DIR

python -u $FACENET_DIR/src/train_tripletloss.py \
	--logs_base_dir $OUTPUT_DIR/facenet_logs/ \
	--models_base_dir $OUTPUT_DIR/facenet_models/ \
	--gpu_memory_fraction 1.0 \
	--data_dir $DATASET_SENADORES_DIR/ \
	--model_def models.inception_resnet_v1 \
	--max_nrof_epochs 500 \
	--batch_size 50 \
	--image_size 160 \
	--people_per_batch 45 \
	--images_per_person 40 \
	--epoch_size  100 \
	--alpha 0.2 \
	--embedding_size 128 \
	--random_crop \
	--random_flip \
	--keep_probability 0.8 \
	--weight_decay 1e-4 \
	--optimizer RMSPROP \
	--learning_rate 0.01 \
	--learning_rate_decay_epochs 2 \
	--learning_rate_decay_factor 1.0 \
	--moving_average_decay  0.9999 \
	--seed 666 \
	--learning_rate_schedule_file $FACENET_DIR/data/learning_rate_schedule_classifier_casia.txt \
	--lfw_pairs $FACENET_DIR/data/pairs.txt \
	#--lfw_file_ext png \
	#--lfw_dir $DATASET_LFW_DIR/ \
	#--lfw_batch_size 100 \
	#--lfw_nrof_folds 10
	#--pretrained_model ../data/output/floyd/facenet_models/20170915-195910/model-20170915-195910.ckpt-49000
