#!/bin/sh

FACENET_DIR=../dev/facenet/src
DATASET_SENADORES_DIR=../data/datasets/senadores/senadores-aligned-mtcnn
#FROZEN_MODEL=../data/output/floyd/facenet_models/20170915-195910/senadores-10k-floyd-20170915.pb
#CLASSIFIER=../data/output/floyd/facenet_models/20170915-195910/senadores-10k-floyd-20170915-classifier.pkl
#DATASET_SENADORES_DIR=../data/datasets/lfw/lfw_mtcnnpy_160/
FROZEN_MODEL=../data/pre-trained-models/20170512-110547/20170512-110547.pb
CLASSIFIER=../data/output/floyd/facenet_models/20170915-195910/senadores-10k-pre-trained-model-20170512-classifier.pkl

python $FACENET_DIR/classifier.py CLASSIFY \
	$DATASET_SENADORES_DIR \
	$FROZEN_MODEL \
	$CLASSIFIER \
	--batch_size 1000 \
	--min_nrof_images_per_class 40 \
	--nrof_train_images_per_class 35 \
	--use_split_dataset
