#!/bin/sh

SENADORES_DIR=../data/datasets/senadores/senadores-aligned-mtcnn

python ../dev/facenet/src/compare.py \
		--image_size 160 \
		--margin 44 \
	        ../data/pre-trained-models/20170512-110547/ \
		$SENADORES_DIR"/Acir Gurgacz/9719462212_426x640_1.png" \
		$SENADORES_DIR"/Acir Gurgacz/9722055073_426x640_1.png" \
		$SENADORES_DIR"/Acir Gurgacz/9722055489_640x426_1.png" \
		$SENADORES_DIR"/Romario/13312220045_1024x681_1.png" \
                $SENADORES_DIR"/Romario/18198156782_1024x680_1.png" \
                $SENADORES_DIR"/Romario/21809158032_1024x504_1.png" \
		$SENADORES_DIR"/Simone Tebet/16157607163_1024x681_1.png" \
                $SENADORES_DIR"/Simone Tebet/17717522150_681x1024_1.png" \
                $SENADORES_DIR"/Simone Tebet/20719621151_1024x681_1.png" 
