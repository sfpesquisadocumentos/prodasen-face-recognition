#!/bin/bash

IMAGE_DOWNLOADER_DIR=./../dev/Image-Downloader
OUTPUT_BASE_DIR=./../data/datasets/senadores/test-dataset
SENADORES="Maria do Carmo Alves,Renan Calheiros,Roberto Requiao,Romero Juca,Jose Agripino,Joao Alberto Souza,Acir Gurgacz,Zeze Perrella,Eduardo Lopes,Jose Medeiros,Regina Sousa,Helio Jose,Dalirio Beber,Pedro Chaves,Roberto Muniz,Airton Sandoval,Flexa Ribeiro,Fernando Collor,Katia Abreu,Cassio Cunha Lima,Wilder Morais,Raimundo Lira,Antonio Anastasia,Jader Barbalho,Garibaldi Alves Filho,Cristovam Buarque,Magno Malta,Lucia Vania,Jose Maranhao,Joao Capiberibe,Tasso Jereissati,Valdir Raupp,Paulo Paim,Ataides Oliveira,Cidinho Santos,Dario Berger,Davi Alcolumbre,Elmano Ferrer,Fatima Bezerra,Fernando Bezerra Coelho,Gladson Cameli,Lasier Martins,Omar Aziz,Otto Alencar,Paulo Rocha,Reguffe,Roberto Rocha,Romario,Ronaldo Caiado,Rose de Freitas,Simone Tebet,Telmario Mota,Wellington Fagundes,Alvaro Dias,Antonio Carlos Valadares,Edison Lobao,Jose Serra,Ana Amelia,Angela Portela,Armando Monteiro,Benedito de Lira,Ciro Nogueira,Eduardo Amorim,Eduardo Braga,Eunicio Oliveira,Gleisi Hoffmann,Humberto Costa,Ivo Cassol,Jorge Viana,Jose Pimentel,Lidice da Mata,Lindbergh Farias,Marta Suplicy,Paulo Bauer,Randolfe Rodrigues,Ricardo Ferraço,Sergio Petecao,Vanessa Grazziotin,Vicentinho Alves,Waldemir Moka,Aecio Neves"
IFS=","

for senador in $SENADORES
do

	python3 $IMAGE_DOWNLOADER_DIR/image_downloader.py \
		--engine Google \
		--max-number 1000 \
		--face-only \
		--output $OUTPUT_BASE_DIR/$senador \
		"Senador "$senador
done
